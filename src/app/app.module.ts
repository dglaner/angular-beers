import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { LoginModule } from './views/login/login.module';
import { RegisterModule } from './views/register/register.module';
import { UsersBeerListModule } from './views/users-beer-list/users-beer-list.module';
import { BeersModule } from './views/beers/beers.module';
import { SharedModule } from './shared/shared.module';
import { CoreGuards } from './guards';
import { CommonModule } from '@angular/common';
import { MatButtonModule, MatIconModule, MatSidenavModule } from '@angular/material';
import { ConfirmationDialogComponent } from './shared/components';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    LoginModule,
    RegisterModule,
    UsersBeerListModule,
    BeersModule,
    SharedModule,
    MatSidenavModule,
    MatButtonModule,
    MatIconModule
  ],
  providers: [
    CoreGuards
  ],
  bootstrap: [AppComponent],
  entryComponents: [ConfirmationDialogComponent]
})
export class AppModule { }
