import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BeersComponents } from './components';
import { AppRoutingModule } from '../../app-routing.module';
import { SharedModule } from '../../shared/shared.module';
import { AgmCoreModule } from '@agm/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import {
  MatAutocompleteModule,
  MatButtonModule, MatButtonToggleModule, MatDialogModule,
  MatInputModule,
  MatMenuModule,
  MatSelectModule, MatSidenavModule, MatSlideToggleModule, MatSnackBarModule,
  MatToolbarModule, MatTooltipModule
} from '@angular/material';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { Ng5SliderModule } from 'ng5-slider';


@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    AppRoutingModule,
    SharedModule,
    FormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDH_UG6T6OVDKFgPONSR7iLmLV60O1KovE'
    }),
    BrowserAnimationsModule,
    BrowserModule,
    MatAutocompleteModule,
    MatInputModule,
    AutocompleteLibModule,
    MatMenuModule,
    MatButtonModule,
    MatSelectModule,
    MatToolbarModule,
    Ng5SliderModule,
    MatButtonToggleModule,
    MatDialogModule,
    MatSlideToggleModule,
    MatTooltipModule,
    MatSnackBarModule,
    MatSidenavModule
  ],
  declarations: [
    ...BeersComponents
  ]
})
export class BeersModule { }
