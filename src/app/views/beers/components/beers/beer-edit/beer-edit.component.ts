import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BeerService, HttpService, MapService, SessionService, TypeService } from '../../../../../shared/services';
import { Beer } from '../../../../../models/beer.model';
import * as moment from 'moment';

@Component({
  selector: 'app-beer-edit',
  templateUrl: './beer-edit.component.html',
  styleUrls: ['./beer-edit.component.scss']
})
export class BeerEditComponent implements OnInit {
  beerId: string;
  id: number;
  editMode = false;
  beerForm: FormGroup;
  selectedTypeName = 'Craft';
  selectedCity = 'Barcelona';
  beer: Beer;
  cityPool;
  constructor(private route: ActivatedRoute,
              private beerService: BeerService,
              private router: Router,
              private httpService: HttpService,
              private typeService: TypeService,
              private mapService: MapService,
              private sessionService: SessionService) { }

  ngOnInit() {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.id = +params['id'];
          this.editMode = params['id'] != null;
          this.initForm();
          this.beer = this.beerService.getBeer(this.id);
        });
  }
  private initForm() {
    this.cityPool = this.mapService.getCityPool();
    let beerName = '';
    let beerImage_url = '';
    let beerDescription = '';
    let beerContributed_by = '';
    let beerTagline = '';
    let beerFirst_brewed = '';
    let beerAbv: number;
    let beerIbu: number;
    let beerEbc: number;
    let beerSrm: number;
    let beerPh: number;

    if (this.editMode) {
      const beer = this.beerService.getBeer(this.id);
      this.selectedTypeName = beer.type.name;
      this.selectedCity = beer.city.name;
      const dateFormat = new Date(beer.first_brewed);
      const month = dateFormat.getMonth();
      let newMonth;
      if (month < 9) {
        newMonth = '0' + (month + 1);
      } else {
        newMonth = month + 1;
      }
      this.beerId = beer._id;
      beerName = beer.name;
      beerImage_url = beer.image_url;
      beerDescription = beer.description;
      beerContributed_by = beer.contributed_by;
      beerTagline = beer.tagline;
      beerFirst_brewed = newMonth + '-' + dateFormat.getFullYear();
      beerAbv = beer.abv;
      beerIbu = beer.ibu;
      beerEbc = beer.ebc;
      beerSrm = beer.srm;
      beerPh = beer.ph;
    }

    this.beerForm = new FormGroup({
      'name': new FormControl(beerName, [Validators.required, Validators.maxLength(60), Validators.minLength(3)]),
      'image_url': new FormControl(beerImage_url, Validators.required),
      'description': new FormControl(beerDescription, Validators.required),
      'contributed_by': new FormControl(beerContributed_by, [Validators.required, Validators.maxLength(60), Validators.minLength(3)]),
      'tagline': new FormControl(beerTagline, [Validators.required, Validators.maxLength(150), Validators.minLength(3)]),
      'first_brewed': new FormControl(beerFirst_brewed, [Validators.required, Validators.pattern('(0[1-9]|1[0-2])\\-(1[0-9]{3}|20[0-1][0-9])')]),
      'abv': new FormControl(beerAbv, [Validators.required, Validators.pattern('^[0-9]\\d*(\\.\\d+)?$'), Validators.maxLength(5), Validators.max(15), Validators.min(0)]),
      'ibu': new FormControl(beerIbu, [Validators.required, Validators.pattern('^[0-9]\\d*(\\.\\d+)?$'), Validators.maxLength(5), Validators.max(1200), Validators.min(1)]),
      'ebc': new FormControl(beerEbc, [Validators.required, Validators.pattern('^[0-9]\\d*(\\.\\d+)?$'), Validators.maxLength(5), Validators.max(600), Validators.min(1)]),
      'srm': new FormControl(beerSrm, [Validators.required, Validators.pattern('^[0-9]\\d*(\\.\\d+)?$'), Validators.maxLength(5), Validators.max(110), Validators.min(1)]),
      'ph': new FormControl(beerPh, [Validators.required, Validators.pattern('^[0-9]\\d*(\\.\\d+)?$'), Validators.maxLength(5), Validators.max(6), Validators.min(3)]),
    });
  }

  async onSubmit() {
    const dateISO = moment(this.beerForm.value['first_brewed'], 'MM-YYYY').toISOString();
    const typeURL = this.typeService.getUrl(this.selectedTypeName);
    const userLoggedIn = this.sessionService.getUser();
    const cityObject = this.cityPool.filter(item => {
      return this.selectedCity === item.name;
    });
    let newBeer: Beer;
    const fetchedBeers = await this.httpService.getListOfBeers();
    if (userLoggedIn.role === 'admin') {
      const creator = { email: userLoggedIn.email, hidden: false};
      if (this.editMode) {
        newBeer = new Beer(this.beerId, this.beerForm.value['name'], this.beerForm.value['description'],
          this.beerForm.value['image_url'], this.beerForm.value['tagline'], dateISO,
          this.beerForm.value['abv'], this.beerForm.value['ibu'], this.beerForm.value['ebc'], this.beerForm.value['srm'],
          this.beerForm.value['ph'], this.beerForm.value['contributed_by'],
          { name: cityObject[0].name, lat: cityObject[0].lat, lng: cityObject[0].lng },
          { name: this.selectedTypeName, image_url: typeURL[0].image_url, color: typeURL[0].color },
          creator);
        await this.beerService.updateBeer(this.id, newBeer);

        for (let i = 0; i < fetchedBeers.length; i++) {
          if (fetchedBeers[i].name === this.beer.name && fetchedBeers[i].creator.hidden === this.beer.creator.hidden) {
            fetchedBeers[i] = newBeer;
          }
        }
        this.httpService.saveBeers(fetchedBeers);
      } else {
        newBeer = new Beer('', this.beerForm.value['name'], this.beerForm.value['description'],
          this.beerForm.value['image_url'], this.beerForm.value['tagline'], dateISO,
          this.beerForm.value['abv'], this.beerForm.value['ibu'], this.beerForm.value['ebc'], this.beerForm.value['srm'],
          this.beerForm.value['ph'], this.beerForm.value['contributed_by'],
          { name: cityObject[0].name, lat: cityObject[0].lat, lng: cityObject[0].lng },
          { name: this.selectedTypeName, image_url: typeURL[0].image_url, color: typeURL[0].color},
          creator);
        const beerAdded = await this.beerService.addBeerAsLoggedIn(newBeer);
        if (!beerAdded) {
          await fetchedBeers.push(newBeer);
          await this.httpService.saveBeers(fetchedBeers);
        }
      }
    } else {
      const creator = { email: userLoggedIn.email, hidden: true};
      if (this.editMode) {
        newBeer = new Beer(this.beerId, this.beerForm.value['name'], this.beerForm.value['description'],
          this.beerForm.value['image_url'], this.beerForm.value['tagline'], dateISO,
          this.beerForm.value['abv'], this.beerForm.value['ibu'], this.beerForm.value['ebc'], this.beerForm.value['srm'],
          this.beerForm.value['ph'], this.beerForm.value['contributed_by'],
          { name: cityObject[0].name, lat: cityObject[0].lat, lng: cityObject[0].lng },
          { name: this.selectedTypeName, image_url: typeURL[0].image_url, color: typeURL[0].color },
          creator);
        await this.beerService.updateBeer(this.id, newBeer);

        for (let i = 0; i < fetchedBeers.length; i++) {
          if (fetchedBeers[i].name === this.beer.name && fetchedBeers[i].creator.hidden === this.beer.creator.hidden) {
            fetchedBeers[i] = newBeer;
          }
        }
        this.httpService.saveBeers(fetchedBeers);
      } else {
        newBeer = new Beer('', this.beerForm.value['name'], this.beerForm.value['description'],
          this.beerForm.value['image_url'], this.beerForm.value['tagline'], dateISO,
          this.beerForm.value['abv'], this.beerForm.value['ibu'], this.beerForm.value['ebc'], this.beerForm.value['srm'],
          this.beerForm.value['ph'], this.beerForm.value['contributed_by'],
          { name: cityObject[0].name, lat: cityObject[0].lat, lng: cityObject[0].lng },
          { name: this.selectedTypeName, image_url: typeURL[0].image_url, color: typeURL[0].color},
          creator);
        const beerAdded = await this.beerService.addBeerAsLoggedIn(newBeer);
        if (!beerAdded) {
          fetchedBeers.push(newBeer);
          this.httpService.saveBeers(fetchedBeers);
        }
      }
    }

    this.router.navigate(['../'], {relativeTo: this.route});
  }
  onBack() {
    this.router.navigate(['../'], {relativeTo: this.route});
  }
}
