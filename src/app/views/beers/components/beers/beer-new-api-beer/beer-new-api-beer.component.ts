import { Component, OnInit } from '@angular/core';
import { BeerService, HttpService, MapService, SessionService } from '../../../../../shared/services';
import { Beer } from '../../../../../models/beer.model';
import { FormControl } from '@angular/forms';
import { TypeService } from '../../../../../shared/services';
import * as moment from 'moment';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-beer-new-api-beer',
  templateUrl: './beer-new-api-beer.component.html',
  styleUrls: ['./beer-new-api-beer.component.scss']
})
export class BeerNewApiBeerComponent implements OnInit {
  messageStatus = 'none';
  messageText: string;
  beerCtrl = new FormControl();
  filteredBeers;
  beerSelected;

  constructor(private httpService: HttpService,
              private mapService: MapService,
              private beerService: BeerService,
              private typeService: TypeService,
              private sessionService: SessionService,
              private _snackBar: MatSnackBar) {
  }
  ngOnInit(): void {
    this.beerCtrl.valueChanges
      .subscribe(name => {
        if (name !== '') {
          this.httpService.searchApiBeer(name)
            .subscribe(searchedBeers => {
              this.filteredBeers = searchedBeers;
            });
        }
      })
  }
  async onSubmit() {
      const randomCity = await this.mapService.getRandomCoords();
      const randomType = await this.typeService.getRandomType();
      const dateISO = await moment(this.beerSelected[0].first_brewed, 'MM-YYYY').toISOString();
      const creator = await this.sessionService.getUser();
      let beerObject;
      if (creator.role === 'user') {
        beerObject = new Beer('', this.beerSelected[0].name, this.beerSelected[0].description, this.beerSelected[0].image_url,
          this.beerSelected[0].tagline, dateISO, this.beerSelected[0].abv, this.beerSelected[0].ibu,
          this.beerSelected[0].ebc, this.beerSelected[0].srm, this.beerSelected[0].ph, this.beerSelected[0].contributed_by, randomCity, randomType, { email: creator.email, hidden: true });
      } else {
        beerObject = new Beer('', this.beerSelected[0].name, this.beerSelected[0].description, this.beerSelected[0].image_url,
          this.beerSelected[0].tagline, dateISO, this.beerSelected[0].abv, this.beerSelected[0].ibu,
          this.beerSelected[0].ebc, this.beerSelected[0].srm, this.beerSelected[0].ph, this.beerSelected[0].contributed_by, randomCity, randomType, { email: creator.email, hidden: false });
      }
      const beerExists = await this.beerService.addBeerAsLoggedIn(beerObject);
      if (!beerExists) {
        const beersToSave = await this.httpService.getListOfBeers();
        beersToSave.push(beerObject);
        await this.httpService.saveBeers(beersToSave);
        this._snackBar.open('Beer successfully added!', 'Accepted', {
          duration: 5000,
          panelClass: ['snackbar']
        });
      } else {
        this._snackBar.open('Beer already exists!', 'Error', {
          duration: 5000,
          panelClass: ['snackbar']
        });
      }
      this.beerSelected = undefined;
  }

  onOptionSelected(value: any) {
    this.beerSelected = this.filteredBeers.filter(item => {
      return item.name === value;
    });
  }
}
