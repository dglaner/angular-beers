import { Component, EventEmitter, OnDestroy, OnInit } from '@angular/core';
import { BeerService, HttpService, MapService, SessionService, TypeService } from '../../../../shared/services';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Beer } from '../../../../models/beer.model';
import { Subscription } from 'rxjs/Rx';

@Component({
  selector: 'app-beers',
  templateUrl: './beers.component.html',
  styleUrls: ['./beers.component.scss']
})
export class BeersComponent implements OnInit, OnDestroy {
  beers: Beer[];
  citiesToDisplay = [];
  typesToDisplay = [];
  cityToFilter = 'all';
  typeToFilter = 'all';
  filterElement = [
    {
      value: 0,
      highValue: 15,
      options: {
        floor: 0,
        ceil: 15,
        step: 0.1
      },
    },
    {
      value: 1,
      highValue: 1200,
      options: {
        floor: 1,
        ceil: 1200,
      },
    },
    {
      value: 1,
      highValue: 600,
      options: {
        floor: 1,
        ceil: 600,
      },
    },
    {
      value: 1,
      highValue: 110,
      options: {
        floor: 1,
        ceil: 110,
      },
    },
    {
      value: 3,
      highValue: 6,
      options: {
        floor: 3,
        ceil: 6,
        step: 0.1
      },
    }];
  manualRefresh: EventEmitter<void> = new EventEmitter<void>();
  beer: Beer;
  mapLat = 47.4925;
  mapLng = 19.051389;
  zoom = 4;
  markers = [];
  icon;
  id: number;
  beersSubscription: Subscription;
  mouseOverSubscription: Subscription;
  onBeerZoomSubscription: Subscription;
  zoomSubscription: Subscription;
  allButtonStyle = {
    'background': 'linear-gradient(to right, #ba68c8, #9c27b0)',
    'color': '#ffffff',
  };
  activeType: number = null;
  mapPool;
  typePool;

  constructor(private httpService: HttpService,
              private beerService: BeerService,
              private formBuilder: FormBuilder,
              private sessionService: SessionService,
              private router: Router,
              private route: ActivatedRoute,
              private mapService: MapService,
              private typeService: TypeService) {}

  async ngOnInit() {
      this.beerService.deleteBeers();
      await this.httpService.fetchBeers()
        .then(result => {
          result.forEach(item => {
            this.beerService.addBeer(item);
          })
        });
      this.beers = this.beerService.getBeers();
    if (this.httpService.fetchedUsersBeers === false && this.sessionService.isLoggedIn() === true) {
      this.httpService.fetchUsersBeers();
      this.httpService.fetchedUsersBeers = true;
    }
    const listOfBeers = await this.httpService.getListOfBeers();
    this.citiesToDisplay = [...new Set(listOfBeers.map((obj) => {
      if (!obj.creator.hidden) {
        return obj.city.name
      }
    }))].filter(name => {
      if (name) {
        return name;
      }
    });
    const map = new Map();
    for (const item of listOfBeers) {
      if (!map.has(item.type.name)) {
        map.set(item.type.name, true);
        this.typesToDisplay.push({
          name: item.type.name,
          image_url: item.type.image_url,
          color: item.type.color
        });
      }
    }
    this.makeMarkers();
    this.beersSubscription = this.beerService.beerChanged
      .subscribe(() => {
        this.makeMarkers();
      });
    this.mouseOverSubscription = this.mapService.mouseOverChanged
      .subscribe((beerId) => {
        this.markers.forEach((item) => {
            if (item.animation === 'BOUNCE') {
              item.animation = 'none';
            }
            if (beerId !== undefined) {
              const beer = this.beerService.getBeer(beerId);
              if (beer.name === item.beerName) {
                item.animation = 'BOUNCE';
                item.index = 2;
              }
            }
          });
      });
    this.onBeerZoomSubscription = this.mapService.beerToZoomChanged
      .subscribe((beer) => {
        setTimeout(() => {
          this.mapLat = Number(beer.city.lat);
          this.mapLng = Number(beer.city.lng);
          this.zoom = 7;
        });
        });
    this.zoomSubscription = this.mapService.zoomChanged
      .subscribe((number) => {
        if (this.cityToFilter === 'all') {
          this.mapLat = 47.4925;
          this.mapLng = 19.051389;
          this.zoom = number;
        }
        });
    this.mapPool = this.mapService.getCityPool();
    this.typePool = this.typeService.getTypePool();
  }
  ngOnDestroy(): void {
    this.mouseOverSubscription.unsubscribe();
  }
  async onToggleChangedGroup1(cities: any) {
    this.cityToFilter = cities;
    await this.filtering(false);
  }
  noClickable($event: MouseEvent) {
    $event.stopPropagation();
  }
  async onChangeSliderPosition() {
    await this.filtering(true);
  }
  async filtering(doFilterBySlider: boolean) {
    let allBeers = await this.httpService.getListOfBeers();
      allBeers = await this.filteringBySliders(allBeers);
    if (this.typeToFilter !== 'all') {
      allBeers = await allBeers.filter(beer => {
        return beer.type.name === this.typeToFilter;
      })
    }
    if (this.cityToFilter === 'all') {
      await this.beerService.deleteBeers();
      await allBeers.forEach(item => this.beerService.addBeer(item));
      this.mapLat = 47.4925;
      this.mapLng = 19.051389;
      this.zoom = 4;
    } else {
      const filteredBeersByCity = await allBeers.filter(beer => {
        return beer.city.name === this.cityToFilter;
      });
      const cityCoords = await this.mapService.getCityCoords(this.cityToFilter);
      if (!doFilterBySlider) {
        if (this.zoom === 4) {
          this.zoom = 7;
          this.mapLat = cityCoords[0].lat;
          this.mapLng = cityCoords[0].lng;
        } else {
          this.zoom = 4;
          this.mapLat = cityCoords[0].lat;
          this.mapLng = cityCoords[0].lng;
          setTimeout(() => {
            this.zoom = 7;
          }, 700);
        }
      }
      await this.beerService.deleteBeers();
      await filteredBeersByCity.forEach(item => this.beerService.addBeer(item));
    }
  }
  filteringBySliders(beers) {
    return beers.filter(beer => {
      if (beer.abv >= this.filterElement[0].value &&
        beer.abv <= this.filterElement[0].highValue &&
        beer.ibu >= this.filterElement[1].value &&
        beer.ibu <= this.filterElement[1].highValue &&
        beer.ebc >= this.filterElement[2].value &&
        beer.ebc <= this.filterElement[2].highValue &&
        beer.srm >= this.filterElement[3].value &&
        beer.srm <= this.filterElement[3].highValue &&
        beer.ph >= this.filterElement[4].value &&
        beer.ph <= this.filterElement[4].highValue) {
        return beer;
      }
    });
  }

  makeMarkers() {
    this.markers = [];
    const allBeers = this.beerService.getBeers();
    allBeers.forEach(item => {
      if (!item.creator.hidden) {
        this.markers.push({
          cityName: item.city.name,
          lat: Number(item.city.lat),
          lng: Number(item.city.lng),
          beerName: item.name,
          type: item.type.name,
          animation: 'none',
          index: 1,
          icon: {
            url: item.image_url,
            scaledSize: {
              width: 20,
              height: 60
            }
          }
        });
      }
    });
  }
  async onTypeClick(type: any, i: number) {
    if (type === 'all') {
      this.activeType = i;
      this.allButtonStyle = {
        'background': 'linear-gradient(to right, #ba68c8, #9c27b0)',
        'color': '#ffffff'
      }
    } else {
      this.activeType = i;
      this.allButtonStyle = {
        'background': '#ffffff',
        'color': '#000000',
      };
    }
    this.typeToFilter = type;
    await this.filtering(true);
  }

  citySelectedOnDevice(value: any) {
    this.cityToFilter = value;
    this.filtering(false);
  }

  typeSelectedOnDevice(value: any) {
    this.typeToFilter = value;
    this.filtering(false);
  }

  menuToggle() {
    setTimeout(() => {
      this.manualRefresh.emit()
    }, 200)
  }
}
