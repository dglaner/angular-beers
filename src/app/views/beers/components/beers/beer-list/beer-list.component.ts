import { Component, OnDestroy, OnInit } from '@angular/core';
import { Beer } from '../../../../../models/beer.model';
import { BeerService } from '../../../../../shared/services';
import { Subscription } from 'rxjs/internal/Subscription';

@Component({
  selector: 'app-beer-list',
  templateUrl: './beer-list.component.html',
  styleUrls: ['./beer-list.component.scss']
})
export class BeerListComponent implements OnInit, OnDestroy {
  userRole = '';
  beers: Beer[];
  subscription: Subscription;
  constructor(private beerService: BeerService) { }

  ngOnInit() {
    this.subscription = this.beerService.beerChanged
      .subscribe(
        (beers: Beer[]) => {
          this.beers = beers;
        }
      );
    this.beers = this.beerService.getBeers();
    if (localStorage.getItem('currentUser')) {
      const currentUser = JSON.parse(localStorage.getItem('currentUser'));
      this.userRole = currentUser['role'];
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
