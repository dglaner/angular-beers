import { Component, Input, OnInit } from '@angular/core';
import { Beer } from '../../../../../../models/beer.model';
import { MapService, SessionService } from '../../../../../../shared/services';

@Component({
  selector: 'app-beer-item',
  templateUrl: './beer-item.component.html',
  styleUrls: ['./beer-item.component.scss']
})
export class BeerItemComponent implements OnInit {
  @Input() beer: Beer;
  @Input() index: number;
  user_email;

  constructor(private mapService: MapService,
              private sessionService: SessionService) {}
  ngOnInit() {
    if (this.sessionService.isLoggedIn()) {
      this.user_email = this.sessionService.getEmail();
    }
  }

  onMouseOver() {
    this.mapService.onMouseOver(this.index);
  }

  onMouseLeave() {
    this.mapService.onMouseLeave();
  }
}
