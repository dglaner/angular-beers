import { Component, OnInit } from '@angular/core';
import { Beer } from '../../../../../models/beer.model';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { BeerService, HttpService, MapService, SessionService, UsersBeerListService } from '../../../../../shared/services';
import { AppRouterLinks, AppRouterUrls } from '../../../../../app-routing.config';
import { MatDialog, MatSnackBar } from '@angular/material';
import { ConfirmationDialogComponent } from '../../../../../shared/components';

@Component({
  selector: 'app-beer-detail',
  templateUrl: './beer-detail.component.html',
  styleUrls: ['./beer-detail.component.scss']
})
export class BeerDetailComponent implements OnInit {
  beer: Beer;
  id: number;
  userRole = '';
  appRouterLinks = AppRouterLinks;
  appRouterUrls = AppRouterUrls;
  currentListName;
  header;
  userLoggedIn;

  constructor(private beerService: BeerService,
              private route: ActivatedRoute,
              private router: Router,
              private mapService: MapService,
              private httpService: HttpService,
              private ublService: UsersBeerListService,
              private sessionService: SessionService,
              private _snackBar: MatSnackBar,
              public dialog: MatDialog) { }

  ngOnInit() {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.id = +params['id'];
          this.beer = this.beerService.getBeer(this.id);
        }
      );
    this.header = {
      'background': this.beer.type.color,
    };
    if (this.beer.creator.hidden === false) {
      this.mapService.setBeerToZoom(this.beer);
    }
    if (localStorage.getItem('currentUser')) {
      const currentUser = JSON.parse(localStorage.getItem('currentUser'));
      this.userRole = currentUser['role'];
      this.userLoggedIn = currentUser['email'];
    }
    if (this.sessionService.isLoggedIn()) {
      this.currentListName = this.ublService.getCurrentList().name;
    }
  }

  onEditBeer() {
    this.router.navigate([this.appRouterLinks.EDIT[0]], {relativeTo: this.route});
  }

  onDeleteBeer() {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: 'Are you sure you want to delete this beer?'
    });
    dialogRef.afterClosed().subscribe(async result => {
      if (result) {
        await this.beerService.deleteBeer(this.id);
        const beersToSave = await this.beerService.getBeers();
        await this.httpService.saveBeers(beersToSave);
        this.router.navigate([this.appRouterLinks.DEFAULT[0]]);
        this.mapService.setZoom(4);
        this._snackBar.open('Beer ' + this.beer.name + ' has been removed!', 'Deleted', {
          duration: 5000,
          panelClass: ['snackbar']
        });
      }
    });
  }
  async onAddToUsersList() {
    const addResult = await this.beerService.addBeerToFavouriteList(this.beer);
    this.httpService.saveUsersBeers();
    if (addResult) {
      this._snackBar.open('Added ' + this.beer.name + ' to ' + this.currentListName + ' list!', 'Accepted', {
        duration: 5000,
        panelClass: ['snackbar']
      });
    } else {
      this._snackBar.open('Beer ' + this.beer.name + ' is on the list already!', 'Error', {
        duration: 5000,
        panelClass: ['snackbar']
      });
    }
  }

  onBack() {
    this.mapService.setZoom(4);
  }

  async onPublish() {
    const result = await this.beerService.publishBeer(this.id, this.beer);
    if (result) {
      this._snackBar.open('Beer has been published!', 'Accepted', {
        duration: 5000,
        panelClass: ['snackbar']
      });
      const beersToSave = await this.beerService.getBeers();
      await this.httpService.saveBeers(beersToSave);
    } else {
      this._snackBar.open('Beer cannot be published. Beer with that name already exists!', 'Error', {
        duration: 8000,
        panelClass: ['snackbar']
      });
    }
  }
}
