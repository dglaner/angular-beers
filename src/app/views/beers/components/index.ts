import { BeersComponent } from './beers/beers.component';
import { BeerDetailComponent } from './beers/beer-detail/beer-detail.component';
import { BeerEditComponent } from './beers/beer-edit/beer-edit.component';
import { BeerListComponent } from './beers/beer-list/beer-list.component';
import { BeerNewApiBeerComponent } from './beers/beer-new-api-beer/beer-new-api-beer.component';
import { BeerItemComponent } from './beers/beer-list/beer-item/beer-item.component';

export const BeersComponents = [
  BeersComponent,
  BeerDetailComponent,
  BeerEditComponent,
  BeerListComponent,
  BeerItemComponent,
  BeerNewApiBeerComponent,
];

export {
  BeersComponent,
  BeerDetailComponent,
  BeerEditComponent,
  BeerListComponent,
  BeerItemComponent,
  BeerNewApiBeerComponent,
}
