import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersBeerListComponents } from './components';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { FlexModule } from '@angular/flex-layout';
import { MatFormFieldModule, MatInputModule, MatOptionModule, MatSelectModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SharedModule,
    FormsModule,
    FlexModule,
    MatFormFieldModule,
    MatOptionModule,
    MatSelectModule,
    MatInputModule
  ],
  declarations: [
    ...UsersBeerListComponents
  ]
})
export class UsersBeerListModule { }
