import { Component, Input, OnInit } from '@angular/core';
import { Beer } from '../../../../../models/beer.model';
import { HttpService, UsersBeerListService } from '../../../../../shared/services';


@Component({
  selector: 'app-users-beer-list-item',
  templateUrl: './users-beer-list-item.component.html',
  styleUrls: ['./users-beer-list-item.component.scss']
})
export class UsersBeerListItemComponent implements OnInit {

  @Input() favouriteBeer: Beer;
  @Input() index: number;
  border;
  constructor(private ublService: UsersBeerListService,
              private httpService: HttpService) { }

  ngOnInit() {
    this.border = {
      'border': '1.4px solid ' + this.favouriteBeer.type.color,
    };  }

  async onDeleteItem() {
    this.ublService.startedEditing = true;
    this.ublService.editingElement = this.index;
    await this.ublService.deleteBeerFromList();
    this.httpService.saveUsersBeers();
  }
}
