import { Component, OnInit } from '@angular/core';
import { HttpService, SessionService, UsersBeerListService } from '../../../../shared/services';
import { List } from '../../../../models/list.model';
import { Subscription } from 'rxjs/internal/Subscription';
import { MatDialog, MatSnackBar } from '@angular/material';
import { ConfirmationDialogComponent } from '../../../../shared/components';


@Component({
  selector: 'app-users-beer-list',
  templateUrl: './users-beer-list.component.html',
  styleUrls: ['./users-beer-list.component.scss']
})
export class UsersBeerListComponent implements OnInit {

  listSelected = 0;
  inputList = '';
  listsOfBeers: List[];
  private listSubscription: Subscription;

  constructor(private ublService: UsersBeerListService,
              private httpService: HttpService,
              private sessionService: SessionService,
              private _snackBar: MatSnackBar,
              public dialog: MatDialog) { }

  ngOnInit() {
    this.listSubscription = this.ublService.listsChanged
      .subscribe((lists: List[]) => {
        this.listsOfBeers = lists;
        this.listSelected = this.ublService.currentList;
      });
    this.listsOfBeers = this.ublService.getLists();
    this.listSelected = this.ublService.currentList;
    if (this.httpService.fetchedUsersBeers === false) {
      this.httpService.fetchUsersBeers();
      this.httpService.fetchedUsersBeers = true;
    }
  }

  onListChange(id: number) {
    this.listSelected = id;
    this.ublService.currentList = this.listSelected;
  }

  addList(value: string) {
    const listLength = this.listsOfBeers.length;
    const email = this.sessionService.getEmail();
    const addListResult = this.ublService.addList(new List(listLength, value, email, []));
    this.inputList = '';
    this.httpService.saveUsersBeers();
    if (addListResult) {
      this._snackBar.open('List successfully added!', 'Accepted', {
        duration: 5000,
        panelClass: ['snackbar']
      });
    } else {
      this._snackBar.open('List already exists!', 'Error', {
        duration: 5000,
        panelClass: ['snackbar']
      });
    }
  }

  onDeleteList() {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: 'Are you sure you want to delete this list?'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        const isDeleted = this.ublService.deleteList();
        if (isDeleted) {
          this.httpService.saveUsersBeers();
          this._snackBar.open('List has been removed!', 'Deleted', {
            duration: 5000,
            panelClass: ['snackbar']
          });
        } else {
          this._snackBar.open('You must have at least one list!', 'Error', {
            duration: 5000,
            panelClass: ['snackbar']
          });
        }
      }
    });
  }
}
