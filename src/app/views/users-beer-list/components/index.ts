import { UsersBeerListComponent } from './user-beer-list/users-beer-list.component';
import { UsersBeerListItemComponent } from './user-beer-list/users-beer-list-item/users-beer-list-item.component';

export const UsersBeerListComponents = [
  UsersBeerListComponent,
  UsersBeerListItemComponent
];

export {
  UsersBeerListComponent,
  UsersBeerListItemComponent
}
