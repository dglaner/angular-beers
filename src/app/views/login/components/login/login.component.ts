import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService, AuthService } from '../../../../shared/services';
import { AppRouterLinks, AppRouterUrls } from '../../../../app-routing.config';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  hide = true;
  email = new FormControl('', [Validators.required, Validators.email]);
  password = new FormControl('', [Validators.required]);
  invalidInputs = false;
  appRouterUrls = AppRouterUrls;
  appRouterLinks = AppRouterLinks;

  constructor(private formBuilder: FormBuilder,
              private userService: UserService,
              private authService: AuthService,
              private router: Router) {}

  ngOnInit() {
  }
  getEmailErrorMessage() {
    return this.email.hasError('required') ? 'Email is required field' :
      this.email.hasError('email') ? 'Email must be valid email' :
        '';
  }
  getPasswordErrorMessage() {
    return this.password.hasError('required') ? 'Password is required field' :
        '';
  }
  onSubmit() {
    if (this.email.invalid || this.password.invalid) {
      this.invalidInputs = true;
    } else {
      this.authService.login(this.email.value, this.password.value)
        .subscribe((res) => {
          this.router.navigate(this.appRouterLinks.DEFAULT);
        }, (err) => {
          this.invalidInputs = true;
        });
    }
  }
}
