import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponents } from './components';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule, MatFormFieldModule, MatIconModule, MatInputModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule
  ],
  declarations: [
    ...LoginComponents
  ]
})
export class LoginModule { }
