import { Component } from '@angular/core';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { AuthService, HttpService, UserService } from '../../../../shared/services';
import { AppRouterLinks, AppRouterUrls } from '../../../../app-routing.config';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {

  message = { text: '', type: ''};
  messageStatus = 'none';
  messageText;
  hide = true;
  email = new FormControl('', [Validators.required, Validators.email]);
  password = new FormControl('', [Validators.required, Validators.pattern('^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\\s).*$'), Validators.minLength(8)]);
  invalidInputs = false;
  appRouterUrls = AppRouterUrls;
  appRouterLinks = AppRouterLinks;

  constructor(private formBuilder: FormBuilder,
              private userService: UserService,
              private httpService: HttpService,
              private authService: AuthService,
              private router: Router) { }

  getEmailErrorMessage() {
    return this.email.hasError('required') ? 'Email is required field' :
      this.email.hasError('email') ? 'Email must be valid email' :
        '';
  }
  getPasswordErrorMessage() {
    return this.password.hasError('required') ? 'Password is required field' :
      this.password.hasError('minlength') ? 'Password must be at least 6 characters' :
        this.password.hasError('pattern') ? 'Password must contain uppercase and lowercase letters and a number' :
        '';
  }
  async onSubmit() {
    if (this.email.invalid || this.password.invalid) {
      this.messageStatus = 'error';
      this.messageText = 'Invalid email or password';
    } else {
      await this.userService.register(this.email.value, this.password.value)
        .subscribe(async (res) => {
          this.httpService.newRegistration(res);
          await this.authService.login(this.email.value, this.password.value)
            .subscribe(() => {
              this.router.navigate(this.appRouterLinks.DEFAULT);
            }, () => {
              this.invalidInputs = true;
            });

        }, (err) => {
          this.messageText = err.error.message;
          this.messageStatus = 'error';
        });
    }
  }
}
