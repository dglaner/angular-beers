import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { SessionService } from '../shared/services';
import { Observable } from 'rxjs/internal/Observable';

@Injectable()
export class UserAuthGuard implements CanActivate {

  constructor(private sessionService: SessionService,
              private router: Router) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    if (this.sessionService.whoAmI() !== 'user' && this.sessionService.whoAmI() !== 'admin') {
      this.router.navigate(['/login']);
      return false;
    }
    return true;
  }
}
