import { AdminAuthGuard } from './admin-auth.guard';
import { UserAuthGuard } from './user-auth.guard';

export const CoreGuards = [
  AdminAuthGuard,
  UserAuthGuard
];

export {
  AdminAuthGuard,
  UserAuthGuard
}
