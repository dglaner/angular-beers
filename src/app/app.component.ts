import { Component, OnInit } from '@angular/core';
import { AppRouterUrls } from './app-routing.config';
import { AuthService, BeerService, HttpService, SessionService, UsersBeerListService } from './shared/services';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  loadedFeature = 'beers';
  appRouterUrls = AppRouterUrls;

  userLoggedIn = false;
  user = {
    email: '',
    role: ''
  };

  constructor(private httpService: HttpService,
              private beerService: BeerService,
              private authService: AuthService,
              private ublService: UsersBeerListService,
              private sessionService: SessionService) {
  }

  ngOnInit() {
    if (this.sessionService.isLoggedIn()) {
      this.authService.setLogin(true);
    }
    this.authService.cast.subscribe(isLoggedIn => {
      this.userLoggedIn = isLoggedIn;
      if (this.userLoggedIn) {
        const currentUser = this.sessionService.getUser();
        this.user.email = currentUser.email;
        this.user.role = currentUser.role;
      }
    });
  }

  onNavigate(feature: string) {
    this.loadedFeature = feature;
  }
}
