import { Beer } from './beer.model';

export class List {
  constructor(
    public id: number,
    public name: string,
    public email: string,
    public beers: Beer[],
  ) {}
}
