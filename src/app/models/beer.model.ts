export class Beer {
  constructor(
    public _id: string,
    public name: string,
    public description: string,
    public image_url: string,
    public tagline: string,
    public first_brewed: string,
    public abv: number,
    public ibu: number,
    public ebc: number,
    public srm: number,
    public ph: number,
    public contributed_by: string,
    public city: {
      name: string,
      lat: number,
      lng: number
    },
    public type: {
      name: string,
      image_url: string,
      color: string
    },
    public creator: {
      email: string,
      hidden: boolean
    }) {}
}
