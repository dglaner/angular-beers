
export const AppRoutes = {
  DEFAULT: 'beers',
  UBL: 'users-beer-list',
  LOGIN: 'login',
  REGISTER: 'register',
  NEWBEER: 'new',
  NEWAPIBEER: 'newApiBeer',
  EDIT: 'edit'
};

export const AppRouterLinks = {
  DEFAULT: [AppRoutes.DEFAULT],
  NEW: [AppRoutes.NEWBEER, AppRoutes.NEWAPIBEER],
  EDIT: [AppRoutes.EDIT]
};

export const AppRouterUrls = {
  DEFAULT: `/${AppRoutes.DEFAULT}`,
  UBL: `/${AppRoutes.UBL}`,
  LOGIN: `/${AppRoutes.LOGIN}`,
  REGISTER: `/${AppRoutes.REGISTER}`,
  NEWBEER: `/${AppRoutes.DEFAULT}/${AppRoutes.NEWBEER}`,
  NEWAPIBEER: `/${AppRoutes.DEFAULT}/${AppRoutes.NEWAPIBEER}`
};
