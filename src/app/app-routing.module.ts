import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {
  BeersComponent,
  BeerEditComponent,
  BeerNewApiBeerComponent,
  BeerDetailComponent,
  BeerListComponent
} from './views/beers/components';
import { UsersBeerListComponent } from './views/users-beer-list/components';
import { LoginComponent } from './views/login/components';
import { RegisterComponent } from './views/register/components';
import { UserAuthGuard } from './guards';

const appRoutes: Routes = [
  { path: '', redirectTo: '/beers', pathMatch: 'full'},
  { path: 'beers', component: BeersComponent, children: [
      { path: '', component: BeerListComponent},
      { path: 'new', component: BeerEditComponent, canActivate: [UserAuthGuard]},
      { path: 'newApiBeer', component: BeerNewApiBeerComponent, canActivate: [UserAuthGuard]},
      { path: ':id', component: BeerDetailComponent},
      { path: ':id/edit', component: BeerEditComponent, canActivate: [UserAuthGuard]}
    ]},
  { path: 'users-beer-list', component: UsersBeerListComponent, canActivate: [UserAuthGuard]},
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
  })
export class AppRoutingModule {

}
