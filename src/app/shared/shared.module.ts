import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent, SharedComponents } from './components';
import { SharedServices } from './services';
import { DropdownDirective } from './dropdown.directive';
import { AppRoutingModule } from '../app-routing.module';
import { MatButtonModule, MatDialogModule, MatListModule, MatMenuModule, MatSidenavModule } from '@angular/material';
import { ConfirmationDialogComponent } from './components';

@NgModule({
  imports: [
    CommonModule,
    AppRoutingModule,
    MatSidenavModule,
    MatListModule,
    MatButtonModule,
    MatMenuModule,
    MatDialogModule
  ],
  declarations: [
    ...SharedComponents,
    DropdownDirective,
    ConfirmationDialogComponent
  ],
  providers: [
    ...SharedServices
  ],
  exports: [
    DropdownDirective,
    HeaderComponent
  ]
})
export class SharedModule { }
