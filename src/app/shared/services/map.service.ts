import { Injectable } from '@angular/core';
import {Subject} from 'rxjs/Rx';
import {Beer} from '../../models/beer.model';

@Injectable()
export class MapService {
  private cityPool = [{name: 'Madrid', lat: 40.383333, lng: -3.7035825}, {name: 'Rome', lat: 41.894802, lng: 12.4853384},
    {name: 'Hamburg', lat: 53.550341, lng: 10.001389}, {name: 'Vienna', lat: 48.2, lng: 16.366667},
    {name: 'Budapest', lat: 47.4925, lng: 19.051389}, {name: 'Barcelona', lat: 41.383333, lng: 2.183333},
    {name: 'Kharkiv', lat: 50.004444, lng: 36.231389}, {name: 'Sofia', lat: 42.6978634, lng: 23.3221789},
    {name: 'Brussels', lat: 50.8467, lng: 4.3525}, {name: 'Cologne', lat: 50.936389 , lng: 6.952778},
    {name: 'Lisbon', lat: 38.7077507, lng: -9.1365919},
    {name: 'Bristol', lat: 51.4538022, lng: -2.5972985}, {name: 'Bordeaux', lat: 44.841225, lng: -0.5800364},
    {name: 'Copenhagen', lat: 55.6867243, lng: 12.5700724}, {name: 'Dublin', lat: 53.3497645, lng: -6.2602732},
    {name: 'Neapol', lat: 40.853330, lng: 14.267990}, {name: 'Tallinn', lat: 59.437790, lng: 24.753581},
    {name: 'Vilnius', lat: 54.685699, lng: 25.286073}, {name: 'Nantes', lat: 47.222550, lng: -1.544287}];

  mouseOver;
  mouseOverChanged = new Subject<number>();
  beerToZoom;
  beerToZoomChanged = new Subject<Beer>();
  zoom;
  zoomChanged = new Subject<number>();

  constructor() {}

  getRandomCoords() {
    const randomNumber = Math.floor((Math.random() * 19));
    return this.cityPool[randomNumber];
  }
  onMouseOver(id: number) {
    this.mouseOver = id;
    this.mouseOverChanged.next(this.mouseOver);
  }
  onMouseLeave() {
    this.mouseOver = undefined;
    this.mouseOverChanged.next(this.mouseOver);
  }
  getCityPool() {
    return this.cityPool;
  }
  getCityCoords(cityName) {
    return this.cityPool.filter(item => {
      return item.name === cityName;
    })
  }
  setBeerToZoom(beer: Beer) {
    this.beerToZoom = beer;
    this.beerToZoomChanged.next(this.beerToZoom);
  }
  setZoom(number) {
    this.zoom = number;
    this.zoomChanged.next(this.zoom);
  }
}
