import { Injectable } from '@angular/core';

@Injectable()
export class TypeService {
  private typePool = [
    { name: 'Porter', image_url: 'https://cdn.dribbble.com/users/10200/screenshots/2952847/pb.jpg', color: '#157CFC'},
    { name: 'IPA', image_url: 'https://image.shutterstock.com/image-vector/ipa-vintage-craft-beer-logo-600w-1282997818.jpg', color: '#D44BD5'},
    { name: 'Lager', image_url: 'https://cdn.freebiesupply.com/logos/large/2x/xx-lager-1-logo-png-transparent.png', color: '#ff9800'},
    { name: 'Craft', image_url: 'https://st2.depositphotos.com/6837936/11079/v/950/depositphotos_110795022-stock-illustration-beer-logo-lettering-for-logotype.jpg', color: '#65AF35'},
    { name: 'Lambic', image_url: 'https://www.lambic.info/images/b/b9/LogoMoeder.jpg', color: '#FD5D21'},
    { name: 'Premium', image_url: 'https://i.etsystatic.com/13221305/r/il/fcc0c5/1385676070/il_794xN.1385676070_5m0a.jpg', color: '#00796B'},
    { name: 'Pale Ale', image_url: 'https://sierranevada.com/wp-content/uploads/2018/10/PaleAle_BottleTapSticker2019_mini2-01.png', color: '#1316bf'}];
  constructor() {}

  getRandomType() {
    const randomNumber = Math.floor((Math.random() * 6));
    return this.typePool[randomNumber];
  }
  getUrl(type: string) {
    return this.typePool.filter(item => {
      return item.name === type;
    });
  }
  getTypePool() {
    return this.typePool;
  }
}
