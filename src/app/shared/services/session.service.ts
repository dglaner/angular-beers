import { Injectable } from '@angular/core';

@Injectable()
export class SessionService {

  constructor() {
  }

  addCurrentUser(user: string) {
    localStorage.setItem('currentUser', user);
  }
  removeCurrentUser() {
    localStorage.removeItem('currentUser');
  }
  getUser() {
    return JSON.parse(localStorage.getItem('currentUser'));
  }
  getEmail(): string {
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));
    return currentUser['email'];
  }
  getToken(): string {
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));
    return currentUser['token'];
  }
  isLoggedIn(): boolean {
    if (localStorage.getItem('currentUser')) {
      return true;
    } else {
      return false;
    }
  }
  whoAmI() {
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (!currentUser) {
      return 'unauthorized';
    }
    return currentUser.role;
  }
}
