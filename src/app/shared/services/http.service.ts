import { Injectable } from '@angular/core';
import { Beer } from '../../models/beer.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UsersBeerListService } from './users-beer-list.service';
import { SessionService } from './session.service';
import { List } from '../../models/list.model';


@Injectable()
export class HttpService {
  fetchedUsersBeers = false;
  serverURL = 'https://beer-api-by-dawid.herokuapp.com/';
  constructor(private httpClient: HttpClient,
              private ublService: UsersBeerListService,
              private sessionService: SessionService) { }

  fetchBeers() {
    return new Promise<Beer[]>(async resolve => {
      const res = await this.httpClient.get(this.serverURL + 'beers/findAll').toPromise();
      const beers: Beer[] = [];
      for (let i = 0; i < Object.keys(res).length; i++) {
          const city = { name: res[i].city.name, lat: res[i].city.lat, lng: res[i].city.lng };
          const type = { name: res[i].type.name, image_url: res[i].type.image_url, color: res[i].type.color};
          const creator = { email: res[i].creator.email, hidden: res[i].creator.hidden };
          const beer = new Beer(res[i]._id, res[i].name, res[i].description, res[i].image_url, res[i].tagline, res[i].first_brewed,
            res[i].abv, res[i].ibu, res[i].ebc, res[i].srm, res[i].ph, res[i].contributed_by, city, type, creator);
          await beers.push(beer);
      }
      resolve(beers);
    });
  }
  getListOfBeers(): Promise<Beer[]> {
    return new Promise(async resolve => {
      const beers: Beer[] = [];
      const res = await this.httpClient.get(this.serverURL + 'beers/findAll').toPromise();
      for (let i = 0; i < Object.keys(res).length; i++) {
        const city = { name: res[i].city.name, lat: res[i].city.lat, lng: res[i].city.lng };
        const type = { name: res[i].type.name, image_url: res[i].type.image_url, color: res[i].type.color};
        const creator = { email: res[i].creator.email, hidden: res[i].creator.hidden };
        let beer;
          beer = new Beer(res[i]._id, res[i].name, res[i].description, res[i].image_url, res[i].tagline, res[i].first_brewed,
            res[i].abv, res[i].ibu, res[i].ebc, res[i].srm, res[i].ph, res[i].contributed_by, city, type, creator);
        await beers.push(beer);
      }
      resolve(beers);
    });
  }
  fetchUsersBeers() {
    let beers: Beer[] = [];
    const userEmail = this.sessionService.getEmail();
    const headers = new HttpHeaders().set('Content-Type', 'application/json')
      .set('authorization', 'Bearer ' + this.sessionService.getToken());
    this.httpClient.post(this.serverURL + 'lists/findLists', { email: userEmail }, { headers: headers }).subscribe((res) => {
      for (let j = 0; j < Object.keys(res).length; j++) {
        res[j].id = j;
        for (let i = 0; i < Object.keys(res[j].beers).length; i++) {
          const city = { name: res[j].beers[i].city.name, lat: res[j].beers[i].city.lat, lng: res[j].beers[i].city.lng };
          const type = { name: res[j].beers[i].type.name, image_url: res[j].beers[i].type.image_url, color: res[j].beers[i].type.color};
          const creator = { email: res[j].beers[i].creator.email, hidden: res[j].beers[i].creator.hidden};
          const addFavBeer = new Beer(res[j].beers[i]._id, res[j].beers[i].name, res[j].beers[i].description, res[j].beers[i].image_url,
            res[j].beers[i].tagline, res[j].beers[i].first_brewed,
            res[j].beers[i].abv, res[j].beers[i].ibu, res[j].beers[i].ebc, res[j].beers[i].srm, res[j].beers[i].ph,
            res[j].beers[i].contributed_by, city, type, creator);
          beers.push(addFavBeer);
        }
        const list = new List(j, res[j].name , res[j].email, beers);
        this.ublService.addList(list);
        beers = [];
      }
    });
  }
  async saveBeers(beersToSave) {
    const headers = await new HttpHeaders().set('Content-Type', 'application/json')
      .set('authorization', 'Bearer ' + this.sessionService.getToken());
    await this.httpClient.post(this.serverURL + 'beers/updateAll', beersToSave, { headers: headers })
      .subscribe(() => {}
      , () => {});
  }
  async saveUsersBeers() {
    const usersLists: any = this.ublService.getLists();
    for (let i = 0; i < Object.keys(usersLists).length; i++) {
      delete usersLists.id;
    }
    const headers = await new HttpHeaders().set('Content-Type', 'application/json')
      .set('authorization', 'Bearer ' + this.sessionService.getToken());
    await this.httpClient.post(this.serverURL + 'lists/updateList', usersLists, { headers: headers })
        .subscribe(() => {
        }, () => {});
  }
  async newRegistration(user) {
    const headers = await new HttpHeaders().set('Content-Type', 'application/json')
      .set('authorization', 'Bearer ' + user.token);
    const emptyList = new List(0, 'All', user.email, []);
    await this.httpClient.post(this.serverURL + 'lists/addList', emptyList, { headers: headers })
      .subscribe(() => {
      });
  }
  searchApiBeer(queryString: string) {
    const baseUrl = 'https://api.punkapi.com/v2/beers?beer_name=';
    const _URL = baseUrl + queryString;
    return this.httpClient.get(_URL);
  }
}
