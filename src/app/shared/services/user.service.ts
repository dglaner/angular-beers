import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../../models/user.model';

@Injectable()
export class UserService {

  constructor(private http: HttpClient) { }

  register(email: string, password: string) {
    const user = new User();
    user.email = email;
    user.password = password;
    user.role = 'user';
    return this.http.post('https://beer-api-by-dawid.herokuapp.com/register', user);
  }

}
