import { AuthService } from './auth.service';
import { BeerService } from './beer.service';
import { HttpService } from './http.service';
import { SessionService } from './session.service';
import { UserService } from './user.service';
import { UsersBeerListService } from './users-beer-list.service';
import { MapService } from './map.service';
import { TypeService } from './type.service';

export const SharedServices = [
  AuthService,
  BeerService,
  HttpService,
  SessionService,
  UserService,
  UsersBeerListService,
  MapService,
  TypeService
];

export {
  AuthService,
  BeerService,
  HttpService,
  SessionService,
  UserService,
  UsersBeerListService,
  MapService,
  TypeService
}
