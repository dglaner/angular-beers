import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { SessionService } from './session.service';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';

@Injectable()
export class AuthService {
  private isLoggedIn = new BehaviorSubject<boolean>(false);
  cast = this.isLoggedIn.asObservable();

  constructor(private http: HttpClient,
              private sessionService: SessionService) { }

  login(email: string, password: string) {
    return this.http.post('https://beer-api-by-dawid.herokuapp.com/login', { email: email, password: password })
      .pipe(map(user => {
        if (user && user['token']) {
          this.sessionService.addCurrentUser(JSON.stringify(user));
          this.isLoggedIn.next(true);
        }
        return user;
      }));
  }
  logout() {
    this.sessionService.removeCurrentUser();
    this.isLoggedIn.next(false);
  }
  setLogin(data: boolean) {
    this.isLoggedIn.next(data);
  }
}
