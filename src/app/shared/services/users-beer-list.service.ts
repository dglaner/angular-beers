import { Beer } from '../../models/beer.model';
import { Injectable } from '@angular/core';
import { List } from '../../models/list.model';
import { Subject } from 'rxjs/internal/Subject';

@Injectable()
export class UsersBeerListService {
  lists: List[] = [];
  listsChanged = new Subject<List[]>();
  currentList = 0;

  startedEditing = false;
  editingElement: number;


  addList(list: List) {
    if (!this.isListDuplicate(list.name)) {
      this.lists.push(list);
      this.listsChanged.next(this.lists.slice());
      return true;
    }
    return false;
  }
  getLists() {
    return this.lists.slice();
  }
  getCurrentList() {
    return this.lists[this.currentList];
  }
  deleteBeerFromList() {
    this.lists[this.currentList].beers.splice(this.editingElement, 1);
    this.listsChanged.next(this.lists.slice());
  }
  addBeerToCurrentList(beer: Beer) {
    if (!this.isBeerDuplicate(beer.name)) {
      this.lists[this.currentList].beers.push(beer);
      this.listsChanged.next(this.lists.slice());
      return true;
    }
    return false;
  }
  clearLists() {
    this.lists = [];
    this.listsChanged.next(this.lists.slice());
  }
  deleteList() {
    if (this.lists.length > 1) {
      this.lists.splice(this.currentList, 1);
      let id = 0;
      this.lists.forEach(listElement => {
        listElement.id = id;
        id++;
      });
      this.currentList = this.lists[0].id;
      this.listsChanged.next(this.lists.slice());
      return true;
      }
    return false;
    }
  isListDuplicate(name) {
    let result = false;
    this.lists.forEach(list => {
      if (list.name.trim() === name.trim()) {
        result = true;
      }
    });
    return result;
  }
  isBeerDuplicate(name) {
    let result = false;
    this.lists[this.currentList].beers.forEach(beer => {
      if (beer.name === name) {
        result = true;
      }
    });
    return result;
  }
}
