import { Beer } from '../../models/beer.model';
import { UsersBeerListService } from './users-beer-list.service';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/internal/Subject';
import { SessionService } from './session.service';
import { HttpService } from './http.service';

@Injectable()
export class BeerService {
  beerChanged = new Subject<Beer[]>();
  private beers: Beer[] = [];

  constructor(private ublService: UsersBeerListService,
              private sessionService: SessionService,
              private httpService: HttpService) {}

  getBeers() {
    return this.beers.slice();
  }
  getBeer(index: number) {
    return this.beers.slice()[index];
  }
  addBeer(beer: Beer) {
    this.beers.push(beer);
    this.beers.sort(this.compare);
    this.beerChanged.next(this.beers.slice());
  }
  async addBeerAsLoggedIn(beer: Beer) {
    const user = this.sessionService.getUser();
    if (user.role === 'admin') {
      const result = await this.isDuplicateForAdmin(beer.name);
      if (!result) {
        this.beers.push(beer);
        this.beers.sort(this.compare);
        this.beerChanged.next(this.beers.slice());
        return false;
      }
    } else {
      const result = await this.isDuplicateForUser(beer.name, user.email);
      if (!result) {
        this.beers.push(beer);
        this.beers.sort(this.compare);
        this.beerChanged.next(this.beers.slice());
        return false;
      }
    }
    return true;
  }
  updateBeer(index: number, newBeer: Beer) {
    this.beers[index] = newBeer;
    this.beerChanged.next(this.beers.slice());
  }
  publishBeer(index: number, newBeer: Beer) {
    let result = true;
    this.beers.forEach(item => {
      if ((item.name === newBeer.name) && (item.creator.hidden === false)) {
        result = false;
      }
    });
    if (result) {
      this.beers[index].creator.hidden = false;
      this.beerChanged.next(this.beers.slice());
    }
    return result;
  }
  deleteBeer(index: number) {
    this.beers.splice(index, 1);
    this.beerChanged.next(this.beers.slice());
  }
  deleteBeers() {
    this.beers.splice(0, this.beers.length);
    this.beerChanged.next(this.beers.slice());
  }

  addBeerToFavouriteList(beer: Beer) {
    return this.ublService.addBeerToCurrentList(beer);
  }
  async isDuplicateForAdmin(name) {
    let result = false;
    const beers = await this.httpService.getListOfBeers();
    beers.forEach(beer => {
      if (beer.name === name && beer.creator.hidden === false) {
        result = true;
      }
    });
    return result;
  }
  async isDuplicateForUser(name, email) {
    let result = false;
    const beers = await this.httpService.getListOfBeers();
    beers.forEach(async (beer) => {
      if ((beer.name === name && beer.creator.hidden === false) ||
        (beer.name === name && beer.creator.email === email && beer.creator.hidden === true)) {
        result = true;
      }
    });
    return result;
  }
  compare(a, b) {
    const nameA = a.name.toUpperCase();
    const nameB = b.name.toUpperCase();

    let comparison = 0;
    if (nameA > nameB) {
      comparison = 1;
    } else if (nameA < nameB) {
      comparison = -1;
    }
    return comparison;
  }
}
