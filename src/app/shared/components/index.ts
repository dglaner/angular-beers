import { HeaderComponent } from './header/header.component';
import {ConfirmationDialogComponent} from './confirmation-dialog/confirmation-dialog.component';

export const SharedComponents = [
  HeaderComponent,
  ConfirmationDialogComponent
];

export {
  HeaderComponent,
  ConfirmationDialogComponent
}
