import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { SessionService, UsersBeerListService, AuthService, BeerService, HttpService } from '../../services';
import { AppRouterUrls } from '../../../app-routing.config';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  appRouterUrls = AppRouterUrls;
  userLoggedIn = false;
  user = {
    email: '',
    role: ''
  };

  @Output() navToggle = new EventEmitter<boolean>();

  constructor(private httpService: HttpService,
              private beerService: BeerService,
              private authService: AuthService,
              private ublService: UsersBeerListService,
              private sessionService: SessionService) {
  }

  ngOnInit() {
    if (this.sessionService.isLoggedIn()) {
      this.authService.setLogin(true);
    }
    this.authService.cast.subscribe(isLoggedIn => {
      this.userLoggedIn = isLoggedIn;
      if (this.userLoggedIn) {
        const currentUser = this.sessionService.getUser();
        this.user.email = currentUser.email;
        this.user.role = currentUser.role;
      }
    });
  }
  sidenavOpen() {
    this.navToggle.emit(true);
  }
  logout() {
    this.authService.logout();
    this.ublService.clearLists();
    this.httpService.fetchedUsersBeers = false;
  }
}

